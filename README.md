** Customer CRUD  application **
** Built with Python 2.7 and pip 9.0.1 **

* Install dependencies:
pip install -r requirements.txt

* Start app:
python main.py
